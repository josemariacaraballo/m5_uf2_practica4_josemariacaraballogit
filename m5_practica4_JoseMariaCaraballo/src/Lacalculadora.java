
import java.util.Scanner;

import javadoc.Empleado;

/**
 * <h2>clase LaCalculadora </h2>
 * 
 * Busca información de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @version 3-2023
 * @author JoseMaria
 * @since 21-2-2023
 */


public class Lacalculadora {
	static Scanner reader = new Scanner(System.in);
	
	/**
	 * Función de una calculadora con sus opciones de sumar, restar, dividir y multiplicar.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+': //Suma 
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-': //Resta
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*': //Multiplicacio
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/': //Divisio
                	
                    
                    	try {
                    		 resultat = divideix(op1, op2);
  
                                  visualitzar(resultat);
                    	}
                      catch (ArithmeticException e) {
						System.out.println("ERROR AL DIVIDIR ENTRE 0");
					}
				
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci� erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	// Métodos públicos
	
	/**
	 * Lee el caracter de la función
	 * @see LaCalculadora
	 * @param car Lee el caracter introducido
	 */
	
	
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}



    /**
	 * Hace la suma
	 * @see calculadora
	 * @param a y b Valor 
	 */
	public static int suma(int a, int b) { /* funci� */
		int res;
		res = a + b;
		return res;
	}

	/**
	 * Hace la resta de los dos valores
	 * @see calculadora
	 * @param a y b Valor 
	 */
	public static int resta(int a, int b) { /* funci� */
		int res;
		res = a - b;
		return res;
	}

	/**
	 * Hace la multiplicacion de las variables
	 * @see calculadora
	 * @param a y b Valor 
	 */
	public static int multiplicacio(int a, int b) { /* funci� */
		int res;
		res = a * b;
		return res;
	}
	/**
	 * Hace la division
	 * @see calculadora
	 * @param a y b Valor 
	 */

	public static int divisio(int a, int b) { /* funci� */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
          
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci� incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	/**
	 * lee el caracter de la funcion
	 * @see calculadora 
	 */
	public static char llegirCar() // funci�
	{
		char car;

		System.out.print("Introdueix un car�cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	/**
	 * Comprueba que el numero es un entero
	 * @return <ul>
	 *                   <li>true: el numero es un entero</li>
	 *                   <li>false: el numero No es un entero</li>
	 *              </ul>
	 */
	public static int llegirEnter() // funci�
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}

	/**
	 * Expone el resultado de la calculadora
	 * @see calculadora
	 * @param res
	 */

	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio �s " + res);
	}
	/**
	 * Expone el menu
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci� i ");
	}
	/**
	 * Hace la division
	 * @see calculadora
	 * @param num1 y num2 Valor 
	 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisió entre zero");
	else 
	res = num1/num2;
		return res;
	}

}
